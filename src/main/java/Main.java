import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvValidationException;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.*;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        // Fifth module
        Properties config = new Properties();
        loadConfiguration(config);

        // First module
        String workingFolderPath = config.getProperty("workingFolder");
        String archiveFolderPath = config.getProperty("archiveFolder");
        String moviesFileLocation = workingFolderPath + "/movies.csv";
        String[] headers = {"movie title", "movie rating", "IMDB link"};

        createFolder(workingFolderPath);
        createFolder(archiveFolderPath);
        createCsvFile(moviesFileLocation, headers);

        // Second module
        final int NUMBER_OF_MOVIES = Integer.parseInt(config.getProperty("numberOfMovies"));
        System.setProperty("webdriver.chrome.driver", config.getProperty("webDriverLocation"));
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        WebDriverWait driverWait = new WebDriverWait(driver, Duration.ofSeconds(30));

        driver.get(config.getProperty("webpageUrl"));

        String openFormTabScript = "window.open('" + config.getProperty("webpageFormUrl") + "', '_blank');";
        ((JavascriptExecutor)driver).executeScript(openFormTabScript);

        String[] tabIds = getTabIds(driver);
        driver.switchTo().window(tabIds[0]);

        navigateToTop250MoviesPage(driver, driverWait);

        List<WebElement> ratings = driver.findElements(By.cssSelector("li.ipc-metadata-list-summary-item.sc-bca49391-0.eypSaE.cli-parent span.ipc-rating-star.ipc-rating-star--base.ipc-rating-star--imdb.ratingGroup--imdb-rating")).stream().limit(NUMBER_OF_MOVIES).toList();
        List<WebElement> links = driver.findElements(By.cssSelector("a.ipc-title-link-wrapper")).stream().limit(NUMBER_OF_MOVIES).toList();

        var titles = getTitles(links);
        writeMovies(ratings, links, titles, moviesFileLocation);
        driver.close();

        // Third module
        driver.switchTo().window(tabIds[1]);
        float averageRating = getAverageRating(moviesFileLocation);
        submitForm(driver, driverWait, averageRating);
        driver.quit();

        // Fourth module
        divideMoviesFile(archiveFolderPath, moviesFileLocation, headers);
    }

    private static void navigateToTop250MoviesPage(WebDriver driver, WebDriverWait driverWait) {
        driver.findElement(By.id("imdbHeader-navDrawerOpen")).click();
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a[href='/chart/top/?ref_=nv_mv_250']")));
        driver.findElement(By.cssSelector("a[href='/chart/top/?ref_=nv_mv_250']")).click();
    }

    private static String[] getTabIds(WebDriver driver) {
        var windowHandles = driver.getWindowHandles();
        var iterator = windowHandles.iterator();
        var tabs = new String[windowHandles.size()];
        for(int i = 0; i < tabs.length; ++i) {
            tabs[i] = iterator.next();
        }
        return tabs;
    }

    private static void submitForm(WebDriver driver, WebDriverWait driverWait, float averageRating) {
        driverWait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input.whsOnd.zHQkBf")));
        driver.findElement(By.cssSelector("input.whsOnd.zHQkBf")).sendKeys(String.valueOf(averageRating));
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.uArJ5e.UQuaGc.Y5sE8d.VkkpIf.QvWxOd span.NPEfkd.RveJvd.snByac")));
        driver.findElement(By.cssSelector("div.uArJ5e.UQuaGc.Y5sE8d.VkkpIf.QvWxOd span.NPEfkd.RveJvd.snByac")).click();
    }

    // Divides movies.csv file into 4 files
    private static void divideMoviesFile(String archiveFolderPath, String moviesFileLocation, String[] headers) {
        List<String[]> content;
        try (CSVReader csvReader = new CSVReader(new FileReader(moviesFileLocation))) {
            String[] header = csvReader.readNext();
            content = csvReader.readAll();
            csvReader.close();

            final int indexOfTitles = findIndexOf(header, "movie title");
            var newFilesContent = divideFileContent(content, indexOfTitles);
            String[] filenames = {"/one word title.csv", "/two words title.csv", "/three words title.csv", "/four+ words title.csv"};
            for(int i = 0; i < filenames.length; ++i) {
                String filepath = archiveFolderPath + filenames[i];
                createCsvFile(filepath, headers);
                writeMovies(newFilesContent.get(i), filepath);
            }
            Files.delete(Paths.get(moviesFileLocation));
        } catch (CsvException | IOException e) {
            handleLogging(e);
        }
    }

    // Divides the movies.csv file content into a List of 4 ArrayList<String[]> based on the number of words
    // the movie has in the title (1, 2, 3, and 4+)
    private static List<ArrayList<String[]>> divideFileContent(List<String[]> content, int indexOfTitles) {
        List<ArrayList<String[]>> newFilesContent = IntStream.range(0, 4).mapToObj(obj -> new ArrayList<String[]>()).toList();

        if(content != null) {
            for (var line : content) {
                int numberOfWordsInTitle = line[indexOfTitles].split(" ").length;
                switch (numberOfWordsInTitle) {
                    case 1 -> newFilesContent.get(0).add(line);
                    case 2 -> newFilesContent.get(1).add(line);
                    case 3 -> newFilesContent.get(2).add(line);
                    default -> newFilesContent.get(3).add(line);
                }
            }
        }
        return newFilesContent;
    }

    private static void loadConfiguration(Properties properties) {
        String configFile = "config.properties";
        try (FileInputStream inputStream = new FileInputStream(configFile)) {
            properties.load(inputStream);
        } catch (IOException e) {
            handleLogging(e);
        }
    }

    private static float getAverageRating(String moviesFileLocation) {
        float sum = 0.0f;
        int counter = 0;

        try (CSVReader csvReader = new CSVReader(new FileReader(moviesFileLocation))) {
            String[] nextLine;
            nextLine = csvReader.readNext();
            final int ratingIndex = findIndexOf(nextLine, "movie rating");
            while ((nextLine = csvReader.readNext()) != null) {
                sum += Float.parseFloat(nextLine[ratingIndex]);
                counter++;
            }
        } catch (CsvValidationException | IOException e) {
            handleLogging(e);
        }

        float averageRating = 0.0f;
        if(counter != 0)
            averageRating = sum / counter;

        return averageRating;
    }

    // Returns the index of the token in the array, if the token can't be found
    // then -1 is returned
    private static int findIndexOf(String[] array, String token) {
        int index = -1;
        for(int i = 0; i < array.length; ++i) {
            if(array[i].equals(token)) {
                index = i;
                break;
            }
        }
        return index;
    }

    private static void writeMovies(List<WebElement> ratings, List<WebElement> links, String[] titles, String filepath) {
        File file = new File(filepath);
        var ratingValues = new String[ratings.size()];

        // Removes number of votes from the rating
        for(int i = 0; i < ratings.size(); ++i) {
            ratingValues[i] = ratings.get(i).getText().split(" ")[0].strip();
        }

        try (CSVWriter writer = new CSVWriter(new FileWriter(file, true))){
            for (int i = 0; i < titles.length; ++i) {
                String[] line = {titles[i], ratingValues[i], links.get(i).getAttribute("href")};
                writer.writeNext(line);
            }
        } catch (IOException e) {
            handleLogging(e);
        }
    }

    private static void writeMovies(List<String[]> content, String filepath) {
        File file = new File(filepath);
        try (CSVWriter writer = new CSVWriter(new FileWriter(file, true))){
            for (String[] strings : content) {
                String[] line = {strings[0], strings[1], strings[2]};
                writer.writeNext(line);
            }
        } catch (IOException e) {
            handleLogging(e);
        }
    }

    // Removes the serial number from the beginning of the title
    private static String[] getTitles(List<WebElement> links) {
        String[] titles = new String[links.size()];
        for (int i = 0; i < titles.length; ++i) {
            titles[i] = links.get(i).getText().split("\\.")[1].trim();
        }
        return titles;
    }

    private static void createCsvFile(String filepath, String[] headers) {
        File file = new File(filepath);
        try (CSVWriter writer = new CSVWriter(new FileWriter(file))) {
            writer.writeNext(headers);
        } catch (IOException e) {
            handleLogging(e);
        }
    }

    private static void createFolder(String path) {
        var folderPath = Paths.get(path);
        try {
            Files.createDirectories(folderPath);
        } catch (IOException e) {
            handleLogging(e);
        }
    }

    // Tries to log the exception e in rpa.log file, if this fails
    // the exception is displayed to the console
    private static void handleLogging(Exception e) {
        Logger logger = Logger.getLogger(Main.class.getName());
        try {
            FileHandler fileHandler = new FileHandler("rpa.log", true);
            logger.addHandler(fileHandler);
            logger.log(Level.SEVERE, "An exception has occurred: ", e);
        } catch (IOException ex) {
            ConsoleHandler consoleHandler = new ConsoleHandler();
            logger.addHandler(consoleHandler);
            logger.log(Level.SEVERE, "An exception has occurred: ", e);
        }
    }
}