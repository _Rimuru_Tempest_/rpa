# RPA

Project developed for an internship, might not work due to changes in the imdb website

## What does it do
Java web scraper developed with Selenium and OpenCSV that scrapes a specified number of movies from the IMDb 
website, saves them to a CSV file based on certain criteria, calculates the average rating, and submits that 
value to a Google form.

## Note
The application might not work due to changes in the imdb website.